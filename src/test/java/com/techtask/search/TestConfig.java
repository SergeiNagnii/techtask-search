package com.techtask.search;

import com.techtask.search.service.impl.DocumentIndexComponent;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

@Configuration
@ActiveProfiles("test")
public class TestConfig {

    @Bean
    public DocumentIndexComponent documentIndexComponent() {
        return Mockito.spy(new DocumentIndexComponent("test"));
    }
}
