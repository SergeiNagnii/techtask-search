package com.techtask.search.service.impl;

import com.techtask.search.BaseTest;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Spy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class DocumentIndexComponentTest extends BaseTest {

    @Spy
    private DocumentIndexComponent documentIndexComponent = new DocumentIndexComponent("test");

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void addTokensToDocFileMap() {
        documentIndexComponent.addTokensToDocFileMap("doc1", "a");
        documentIndexComponent.addTokensToDocFileMap("doc1", "b");
        documentIndexComponent.addTokensToDocFileMap("doc2", "a");

        assertEquals(2, documentIndexComponent.getDocumentIndexMap().size());
        assertEquals(2, documentIndexComponent.getDocumentIndexMap().get("a").size());
        assertEquals(1, documentIndexComponent.getDocumentIndexMap().get("b").size());
    }

    @Test
    public void testIndexExistingDocuments() throws IOException {
        File doc1 = tempFolder.newFile("doc1");
        File doc2 = tempFolder.newFile("doc2");
        FileUtils.write(doc1, "a b c", UTF_8);
        FileUtils.write(doc2, "d e f", UTF_8);
        doReturn(Paths.get(doc1.getParent())).when(documentIndexComponent).getDocumentsPath();

        documentIndexComponent.readAndIndexFiles();

        assertEquals(6, documentIndexComponent.getDocumentIndexMap().size());
    }
}