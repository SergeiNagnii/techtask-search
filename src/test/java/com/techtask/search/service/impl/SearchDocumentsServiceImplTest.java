package com.techtask.search.service.impl;

import com.techtask.search.BaseTest;
import com.techtask.search.dto.DocumentDto;
import com.techtask.search.dto.ResponseDocumentKeysDto;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SearchDocumentsServiceImplTest extends BaseTest {

    @Spy
    @InjectMocks
    private SearchDocumentsServiceImpl searchDocumentsServiceImpl;
    @Mock
    private DocumentIndexComponent documentIndexComponent;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setup() {
        Mockito.when(documentIndexComponent.getDocumentsPath()).thenReturn(Paths.get(tempFolder.getRoot().getAbsolutePath()));
    }

    @Test
    public void createAndFindDocumentsByKey() throws IOException {
        File file = tempFolder.newFile("test-doc");
        FileUtils.write(file, "a b c", UTF_8);
        DocumentDto doc = searchDocumentsServiceImpl.findDocumentByKey("test-doc");
        assertEquals("test-doc", doc.getKey());
        assertEquals("a b c", doc.getContent());
        Files.deleteIfExists(Paths.get("./test"));
    }

    @Test
    public void findDocumentsByEmptyKey() throws IOException {
        DocumentDto doc = searchDocumentsServiceImpl.findDocumentByKey("non-existing-doc");
        assertEquals("", doc.getKey());
        assertEquals("", doc.getContent());
    }

    @Test
    public void findDocumentsByNullKey() throws IOException {
        DocumentDto doc = searchDocumentsServiceImpl.findDocumentByKey(null);
        assertEquals("", doc.getKey());
        assertEquals("", doc.getContent());
    }

    @Test
    public void saveDocument() {
        searchDocumentsServiceImpl.saveDocument("doc1", "a b c");
        verify(documentIndexComponent, times(3)).addTokensToDocFileMap(any(String.class), any(String.class));
        verify(documentIndexComponent).addTokensToDocFileMap("doc1", "a");
        verify(documentIndexComponent).addTokensToDocFileMap("doc1", "b");
        verify(documentIndexComponent).addTokensToDocFileMap("doc1", "c");
    }

    @Test
    public void saveEmptyDocument() {
        searchDocumentsServiceImpl.saveDocument("doc1", "");
        verify(documentIndexComponent, times(0)).addTokensToDocFileMap(any(String.class), any(String.class));
    }

    @Test
    public void saveEmptyNullDocument() {
        searchDocumentsServiceImpl.saveDocument(null, null);
        verify(documentIndexComponent, times(0)).addTokensToDocFileMap(any(String.class), any(String.class));
    }

    @Test
    public void findDocumentsByTokens() {
        HashMap<String, Set<String>> testMap = new HashMap<>();
        Set<String> docsSet = new HashSet<>();
        docsSet.add("test-doc");
        testMap.put("a", docsSet);
        doReturn(testMap).when(documentIndexComponent).getDocumentIndexMap();

        ResponseDocumentKeysDto documentsByTokens = searchDocumentsServiceImpl.findDocumentsByTokens(Arrays.asList("a", "b", "c"));
        assertThat(documentsByTokens.getDocumentKeys().size(), is(1));
        assertEquals(documentsByTokens.getDocumentKeys().iterator().next(), "test-doc");
    }

    @Test
    public void findDocumentsByTokensWhenTokenDoesntHaveDocuments() {
        ResponseDocumentKeysDto documents = searchDocumentsServiceImpl.findDocumentsByTokens(Collections.singletonList("a"));
        assertThat(documents.getDocumentKeys(), empty());
    }
}