package com.techtask.search.service.impl;

import com.techtask.search.BaseITest;
import com.techtask.search.dto.DocumentDto;
import com.techtask.search.dto.ResponseDocumentKeysDto;
import com.techtask.search.service.SearchDocumentsService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class SearchDocumentsServiceImplITest extends BaseITest {

    @Autowired
    private SearchDocumentsService searchDocumentsService;
    @Autowired
    private DocumentIndexComponent documentIndexComponent;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setupFilePath() {
        doReturn(Paths.get(tempFolder.getRoot().getAbsolutePath())).when(documentIndexComponent).getDocumentsPath();
    }

    @Test
    public void saveAndFindDocumentByDocKey() {
        searchDocumentsService.saveDocument("test-doc", "a b c");
        DocumentDto documentByKey = searchDocumentsService.findDocumentByKey("test-doc");

        assertEquals("test-doc", documentByKey.getKey());
        assertEquals("a b c", documentByKey.getContent());
    }

    @Test
    public void saveAndFindDocumentByTokens() {
        searchDocumentsService.saveDocument("test-doc", "x y z");
        ResponseDocumentKeysDto documentsByTokens = searchDocumentsService.findDocumentsByTokens(Arrays.asList("z", "n"));

        assertEquals(1, documentsByTokens.getDocumentKeys().size());
        assertEquals("test-doc", documentsByTokens.getDocumentKeys().iterator().next());
    }

    @Test
    public void findNonExistingDocByDocKey() {
        DocumentDto document = searchDocumentsService.findDocumentByKey("some doc");
        assertEquals("", document.getKey());
        assertEquals("", document.getContent());
    }

    @Test
    public void findNonExistingDocByTokens() {
        ResponseDocumentKeysDto documentsByTokens = searchDocumentsService.findDocumentsByTokens(Arrays.asList("zzz"));
        assertEquals(0, documentsByTokens.getDocumentKeys().size());
    }
}
