package com.techtask.search.dto;

import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Value
public class DocumentDto {
    @NotNull
    @NotEmpty
    private String key;
    @NotNull
    @NotEmpty
    private String content;
}
