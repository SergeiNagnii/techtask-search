package com.techtask.search.service;

import com.techtask.search.dto.DocumentDto;
import com.techtask.search.dto.ResponseDocumentKeysDto;

import java.util.List;

public interface SearchDocumentsService {

    DocumentDto findDocumentByKey(String documentKey);

    void saveDocument(String docKey, String tokens);

    ResponseDocumentKeysDto findDocumentsByTokens(List<String> tokens);
}
