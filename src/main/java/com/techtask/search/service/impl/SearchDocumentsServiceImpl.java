package com.techtask.search.service.impl;

import com.techtask.search.dto.DocumentDto;
import com.techtask.search.dto.ResponseDocumentKeysDto;
import com.techtask.search.service.SearchDocumentsService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service("searchDocumentsService")
public class SearchDocumentsServiceImpl implements SearchDocumentsService {

    private final Logger logger = LoggerFactory.getLogger(SearchDocumentsServiceImpl.class);
    private final DocumentIndexComponent documentIndexComponent;

    public SearchDocumentsServiceImpl(DocumentIndexComponent documentIndexComponent) {
        this.documentIndexComponent = documentIndexComponent;
    }

    @Override
    public DocumentDto findDocumentByKey(String documentKey) {
        try {
            Path document = getDocumentPath(documentKey);
            if (Files.exists(document)) {
                return new DocumentDto(documentKey, new String(Files.readAllBytes(document), UTF_8));
            }
            return new DocumentDto("", "");
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

//TODO probably should be @Async
    @Override
    public void saveDocument(String docKey, String tokens) {
        if(Objects.isNull(docKey) || Objects.isNull(tokens)) {
            return;
        }
        try {
            saveToFile(docKey, tokens);
            Arrays.stream(tokens.split(" "))
                  .filter(t -> !t.isEmpty())
                  .forEach(t -> documentIndexComponent.addTokensToDocFileMap(docKey, t.toLowerCase()));
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ResponseDocumentKeysDto findDocumentsByTokens(List<String> tokens) {
        return new ResponseDocumentKeysDto(tokens.stream()
                                                 .flatMap(t -> getDocumentsSetByToken(t).stream())
                                                 .collect(Collectors.toSet()));
    }

    private void saveToFile(String docName, String tokens) throws IOException {
        if (Files.notExists(documentIndexComponent.getDocumentsPath())) {
            Files.createDirectory(documentIndexComponent.getDocumentsPath());
        }
        FileUtils.write(getDocumentPath(docName).toFile(), tokens, UTF_8);
    }

    private Path getDocumentPath(String docName) {
        return Paths.get(String.format("%s/%s", documentIndexComponent.getDocumentsPath(), docName));
    }

    private Set<String> getDocumentsSetByToken(String token) {
        return documentIndexComponent.getDocumentIndexMap().getOrDefault(token.toLowerCase(), Collections.emptySet());
    }

}
