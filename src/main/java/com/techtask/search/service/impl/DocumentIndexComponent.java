package com.techtask.search.service.impl;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

import static java.nio.charset.StandardCharsets.UTF_8;

@Profile("prod")
@Component
public class DocumentIndexComponent {

    private final Logger logger = LoggerFactory.getLogger(DocumentIndexComponent.class);
    private final Map<String, Set<String>> documentIndexMap;
    private final String documentsPath;

    public DocumentIndexComponent(@Value("${document.path}") String documentsPath) {
        this.documentsPath = documentsPath;
        this.documentIndexMap = new ConcurrentHashMap<>();
    }

    @PostConstruct
    public void indexDocuments(){
        readAndIndexFiles();
    }

    Map<String, Set<String>> getDocumentIndexMap() {
        return documentIndexMap;
    }

    Path getDocumentsPath() {
        return Paths.get(documentsPath);
    }

    void addTokensToDocFileMap(String docKey, String token) {
        documentIndexMap.putIfAbsent(token, new ConcurrentSkipListSet<>());
        documentIndexMap.get(token).add(docKey);
    }

    void readAndIndexFiles()  {
        try {
            if (Files.exists(getDocumentsPath())) {
                Files.walk(getDocumentsPath()).filter(Files::isRegularFile).forEach(this::indexDocument);
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private void indexDocument(Path doc) {
        try {
            FileUtils.readLines(doc.toFile(), UTF_8)
                     .stream()
                     .flatMap(line -> Arrays.stream(line.split(" ")))
                     .forEach(token -> addTokensToDocFileMap(doc.getFileName().toString(), token.toLowerCase()));
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

}
