package com.techtask.search.controller;

import com.techtask.search.dto.DocumentDto;
import com.techtask.search.dto.ResponseDocumentKeysDto;
import com.techtask.search.service.SearchDocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("documents")
public class DocumentsController {

    private final Logger logger = LoggerFactory.getLogger(DocumentsController.class);
    private final SearchDocumentsService searchDocumentsService;

    public DocumentsController(SearchDocumentsService searchDocumentsService) {
        this.searchDocumentsService = searchDocumentsService;
    }

    @GetMapping("/keys")
    public DocumentDto getDocumentsByKey(@RequestParam String documentKey) {
        return searchDocumentsService.findDocumentByKey(documentKey);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public void saveDocuments(@RequestBody @Valid DocumentDto doc) {
        searchDocumentsService.saveDocument(doc.getKey(), doc.getContent());
    }

    @GetMapping("/tokens")
    public ResponseDocumentKeysDto findDocumentsByTokens(@RequestParam List<String> tokens) {
        return searchDocumentsService.findDocumentsByTokens(tokens);
    }

    @ExceptionHandler
    public ResponseEntity<String> onException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
