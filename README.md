# Tech task - search

### Installing

```
mvn clean install
```

### Running

Run project (either from IDE or by java -jar command)

Also client-search project should be built and started (for accessing endpoints via web)

Endpoints are available on http://localhost:8082/documents